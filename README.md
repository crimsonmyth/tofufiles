# TOFUFILES.

Dotfiles are used to customize your system. The "Dotfiles" name is derived from the configuration files in Unix-like systems that start
with a '.' (e.g. .zshrc, .gitconfig) this indicates that these are not regular documents, and by default are hidden when you try to list
files in a directory with "ls" or "[exa](https://the.exa.website.com)". Dotfiles are a core tool belt for users that really want to make a program their own.

## Prerequisites.
- [zsh](https://zsh.org/).
- [exa](https://the.exa.website.com/).
- [Sxiv](https://github.com/muennich/sxiv/).
- [Neovim](https://neovim.io/). 
- [bat](https://github.com/sharkdp/bat/).
- [NetworkManager](https://networkmanager.dev/).
- [RipGrep](https://github.com/BurntSushi/ripgrep/).
- [NerdFonts](https://nerdfonts.com/).
- [Alacritty](https://alacritty.org/).
- [Stow](https://gnu.org/software/stow/).
- [Git](https://git-scm.com/).
- [Zathura](https://git.pwmt.org/pwmt/zathura/).
- [NNN](https://github.com/jarun/nnn/).
- [Unimatrix](https://github.com/will8211/unimatrix).
- [Starship](https://starship.rs/). Install with the below command: 
```bash
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
```

## Download. 
Clone the Repository: 
```bash
git clone https://gitlab.com/crimsonmyth/tofufiles.git
```

## Install. 
All my dotfiles:
```bash
stow */ -t $HOME # / will ignore the README
```
Install A Particular Configuration:
```bash
stow / alacritty -t $HOME # This will stow my alacritty configuration. 
```
